export default {
    "host": "api", // Using Docker config, remove if not in Docker, only use absolute hosts.
    "port": 8080, // default value
    "path": "model",
    "respondAsText": false, // default value
    "headers": {"content-type": "application/json; charset=utf-8"} //Recommended
};
